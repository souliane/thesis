#!/bin/bash

function error {
    echo $1
    exit 1
}

for dir in `ls OpenStudioModel/results/ | grep ^D`; do

    timesteps=`grep "Number of Timesteps per Hour" OpenStudioModel/results/$dir/in.idf | cut -d ";" -f 1 | xargs`
    if [[ $timesteps != 12 ]]; then
        error "Variant $dir has been simulated with only $timesteps timesteps per hour!"
    fi

    # check that the occupancy values look OK in the in.idf file (there was an issue
    # with the manipulation of the OSM file that leads to incorrect values)
    for count in `grep 'Number of People.$' OpenStudioModel/results/$dir/in.idf | cut -d "," -f 1`; do
        echo "12 13 14 15 17 18 6 6.5 7 7.5 8.5 9" | grep -wq $count || error "Variant $dir uses occupancy value $count which is probably incorrect!"
    done
    
    git add "OpenStudioModel/expanded.$dir.idf"

    for file in eplusout.csv.feather in.idf results.json; do
        git add "OpenStudioModel/results/$dir/$file";
    done
done
